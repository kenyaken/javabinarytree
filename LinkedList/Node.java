class Node <T> {
	private Node<T> head, tail;
	private T data;
	
	public Node () {}
	
	public Node (T data) {
		this.data = data;
	}
	
	public void setHead(Node<T> head) {
		this.head = head;
	}
	
	public void setTail(Node<T> tail) {
		this.tail = tail;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
	public Node<T> getHead() {
		return head;
	}
	
	public Node<T> getTail() {
		return tail;
	}
	
	public T getData() {
		return data;
	}
}
