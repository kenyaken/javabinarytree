class LinkedList <T> {
	private Node<T> head;
	public String toString() {
		StringBuilder s = new StringBuilder("[");

		Node<T> node = head;
		try {
			while (true) {
				s.append(node.getData());
				if (node.getTail() == null) throw new NullPointerException();
				s.append(", ");
				node = node.getTail();
			}
		} catch (NullPointerException e) {}

		s.append("]");

		return s.toString();
	}

	/**
	 * Append data to the end of the list
	 * @param data data to append
	 */
	public void append(T data) {
		if (head == null) {
			head = new Node<>(data);
		} else {
			Node<T> node = head;
			try {
				while (true) {
					if (node.getTail() == null) throw new NullPointerException();
					node = node.getTail();
				}
			} catch (NullPointerException e) {}

			Node<T> newNode = new Node<>(data);
			newNode.setHead(node);
			node.setTail(newNode);
		}
	}

	/**
	 * Delete data from the list
	 * @param data data to delete
	 * @throws Exception in case the data isn't found in the list
	 */
	public void delete(T data) throws Exception {
		if (head == null) {
			throw new Exception();
		} else {
			// check if there is only one node i.e the head node with data in it
			// if that is the case then make the head/linkedlist as null.
			if (head.getData() == data) {
				head = null;
			} else {
				// CREATE NEW NODES; CURRENTNODE(HOLDS THE HEAD),PREVNODE(HOLDS THE PREVIOUSNODE),NEXTNODE;
				Node<T> currentNode = head;
				Node<T> prevNode = head, nextNode = head;
				while (currentNode.getData() != data) {
					//check if the nodes are two set the currentnode's tail to null i.e remain with the head node;
					if (currentNode.getTail().getTail() == null) {
						//check wether the next node has data or not,if not throw an exception
						if (currentNode.getTail().getData() != data) {
							throw new Exception();
							//if this condition is true set the head to null
						} else {
							currentNode.setTail(null);
						}
						return;
					}
					// populate the nodes and loop through it till you get the node equal to the data;
					prevNode = currentNode; //assign currentnode to be prevnode
					//nextNode = currentNode.getTail().getTail(); // nextnode currentnode.next.next cn--->n
					currentNode = currentNode.getTail();//this is the newnode becomes the currentnode
					nextNode = currentNode.getTail();
				}
				// since we are deleting. the previous node is set to the nextnode and the head of the next node pointing to the previous node
				prevNode.setTail(nextNode);
				nextNode.setHead(prevNode);
			}
		}
		
	}
	// a method to insert the data at the top/as the head of the list
	public void insertTop(T data){
		//check if linkedlist/head is empty
		if (head ==null){
			head = new Node<>(data);
		}else{
			// if head is not null empty create a new node and populate it with data
			// point the head to the new node created
			//point the tail to the head
			Node<T> newnode = new Node<>(data);
			newnode.setTail(head);
			head.setHead(newnode);

			head = newnode;
		}

	}
	// this is a method to delete the data at a given index; index is of type integer;
	public void deleteAt(int index){
		if (head == null){
			return;
		}else{
			if (index==0){
				Node<T> current = head;
				Node<T> next = head;
				next = current.getTail();
				head = next;
			}else{
				Node<T> current = head;
				Node<T> prev = head, next = head;
				for (int i = 0;i<index;i++){
					prev=current;
					current = current.getTail();
					next = current.getTail();
				}
				prev.setTail(next);
				next.setHead(prev);
			}

		}
	}

	/**
	 * Insert data at a specific location in the list
	 * @param data data to insertAt
	 * @param index location at which to do the insertion
	 */
	public void insertAt(T data, int index) {
		// if the head is null create a new node and populate it with data
		if (head == null) {
			head = new Node<>(data);
			//else insertAt data at position {index}
		} else if (index == 0) {
			insertTop(data);
		} else {
			// create current,prev and nextnodes;
			Node<T> currentNode = head;
			Node<T> prevNode = head, nextNode = head;
			//loop through to the index passed
			for (int i = 0; i < index; i++) {
				// assign currentNode to previosnode
				prevNode = currentNode;
				//next node is same as currentnode.next.next
				nextNode = currentNode.getTail().getTail();
				// current node address is assigned to currentnode.getTail();
				currentNode = currentNode.getTail();
			}
			//create a newnode to hold the data
			Node<T> newNode = new Node<>(data);
			//set the head of the new node to the previous node
			newNode.setHead(prevNode);
			//set the tail of the newnode with current node address aBOVE
			newNode.setTail(currentNode);
			// set the tail of the previous node to the new node;
			prevNode.setTail(newNode);

			currentNode.setHead(newNode);
			currentNode.setTail(nextNode);
			nextNode.setHead(currentNode);
		}
	}
	public int length(){
		int size=1;
		if (head==null)
			return 0;
		else{
			Node <T> current = head;
			while (current.getTail()!=null){
				current =current.getTail();
				size++;
			}
			return size;
		}
	}

	/**
	 * Get data stored at a specific location
	 * @param index location to check
	 * @return the data that's been found
	 */
	public T getDataAtIndex(int index) {
		if (head == null) {
			return null;
		} else {
			Node<T> currentNode = head;
			for (int i = 0; i < index; i++) {
				currentNode = currentNode.getTail();
			}
			
			return currentNode.getData();
		}
	}
	
}
